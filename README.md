# UniversalJavaLauncher
Universal Java launcher or UJL. This is a simple java program launcher. In the future it will download jdk or java libs if you need, but now it is a base for smth bigger.


# **How to use it?**
You need to move your **.jar** file near executable **ujl** file and create **default.conf** file. Open it in your text editor (for example **nano or vim**) and write inside java jar running command. **For example**:

`java -jar UJL.jar`

# **How to install it**
**Linux:**

Go to release-linux branch and read a new instruction inside.

# **How to build it?**
Just open it as **VS Code** project and install all extensions.

After go to **build** folder and run **ujl**