package com.ujl;

import javax.swing.*;

public class SimpleSwingApp {
    public static void main(String[] args) {
        // Create a frame (window)
        JFrame frame = new JFrame("Simple Swing App");

        // Create a label
        JLabel label = new JLabel("Hello, Swing!");

        // Create a button
        JButton button = new JButton("Click Me");

        // Add the label and button to the frame
        frame.getContentPane().add(label);
        frame.getContentPane().add(button);

        // Set frame size
        frame.setSize(300, 200);

        // Set frame close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Make the frame visible
        frame.setVisible(true);
    }
}
