#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

std::string line;
std::string line2;
std::string result = "";


// Function to extract Java version number from the output string
std::string extractJavaVersion(const std::string& output) {
    std::string version;
    size_t found = output.find("version");
    if (found != std::string::npos) {
        size_t start = found + 9; // Length of "version" is 7 plus 1 for the space
        size_t end = output.find_first_of("\"", start);
        if (end != std::string::npos) {
            version = output.substr(start, end - start);
        }
    }
    return version;
}


std::string getJavaVersion(){
    // Open a pipe to run the command
    FILE* pipe = popen("java -version 2>&1", "r");
    if (!pipe) {
        std::cerr << "Error executing command." << std::endl;
        return "Error: no java has been installed or program needs another version!";
    }

    // Read command output
    char buffer[128];
    while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != nullptr)
            result += buffer;
    }

    // Close the pipe
    pclose(pipe);

    // Extract Java version number from the output
    std::string version = extractJavaVersion(result);

    // Print the Java version number
    if (!version.empty()) {
        std::cout << "Java version number: " << version << std::endl;
    } else {
        std::cerr << "Java version not found." << std::endl;
    }
    return version;
}


int config(){
    // Open the file
    std::ifstream file("default.conf");

    // Check if the file is opened successfully
    if (!file.is_open()) {
        std::cerr << "Error opening config file." << std::endl;
        return 1;
    }

    if (std::getline(file, line)) {
        // Print the first line
        std::cout << "First line read from file: " << line << std::endl;

        // Read the second line from the file
        if (std::getline(file, line2)) {
            // Print the second line
            std::cout << "Second line read from file: " << line2 << std::endl;
        } else {
            //std::cerr << "Error reading second line from file." << std::endl;
            return 1;
        }
    } else {
        std::cerr << "Error reading first line from file." << std::endl;
        return 1;
    }


    // Close the file
    file.close();

    return 0;
}


int main(int, char**) {

    std::string java_version = getJavaVersion();

    config();
    const char* ln = line.c_str();
    std::system(ln);

    return 0;
}
